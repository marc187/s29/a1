const express = require(`express`);
const app = express();
const PORT = 5000;

app.use(express.json())
app.use(express.urlencoded({extended:true}))

app.listen(PORT, console.log(`Listening to port ${PORT}`))

const users = [
    {
        "userName": "johndoe",
        "password": "johndoe1234"
    }
]
app.get("/home", (req, res) => {
    res.send("Welcome to home page.")
})

app.get("/users", (req, res) => {
    console.log(users)
    res.send("These are the users.")
})

app.delete("/delete-user", (req, res) => {
    const {userName} = req.body;
    users.forEach( user => {
        if(userName && userName == user.userName){
            users.splice(users.indexOf(user.userName), 1);
            res.send(`User ${userName} has been deleted.`);
        } else {
            res.status(500);
            res.send(`${userName} doesn't exist.`);
        }
    })
    console.log(users)
})